FROM centos:7.3.1611

MAINTAINER Dzung Kiu <dzungkiu@gmail.com>

ENV NPS_VERSION=1.12.34.2 \
    NGINX_VERSION=1.13.0 \
    NGINX_USER=www \
    NGINX_SITECONF_DIR=/etc/nginx/sites-enabled \
    NGINX_LOG_DIR=/var/log/nginx \
    NGINX_TEMP_DIR=/var/lib/nginx \
    NGINX_SETUP_DIR=/var/cache/nginx

RUN yum update --exclude=iputils -y

# Set up user
RUN useradd -u 5353 ${NGINX_USER}

# Install and build and remove in one step
# All this needs to be done in one RUN command to conserve space in the final image.
RUN yum install gcc-c++ pcre-devel zlib-devel make gettext unzip -y; \
    curl -L https://github.com/pagespeed/ngx_pagespeed/archive/v${NPS_VERSION}-beta.zip >> release-${NPS_VERSION}-beta.zip; \
    unzip release-${NPS_VERSION}-beta.zip; \
    curl -L https://dl.google.com/dl/page-speed/psol/${NPS_VERSION}-x64.tar.gz >> ${NPS_VERSION}.tar.gz; \
    tar -xzvf ${NPS_VERSION}.tar.gz -C ngx_pagespeed-${NPS_VERSION}-beta; \
    curl -L http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz >> nginx-${NGINX_VERSION}.tar.gz; \
    tar -xvzf nginx-${NGINX_VERSION}.tar.gz; \
    cd nginx-${NGINX_VERSION}; ./configure  --prefix=/etc/nginx --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log  --user=${NGINX_USER} --group=${NGINX_USER} --sbin-path=/usr/sbin/nginx --add-module=/ngx_pagespeed-${NPS_VERSION}-beta --with-http_realip_module; \
    make; \
    make install; \
    cd /; rm -Rf ngx_pagespeed-${NPS_VERSION}-beta/; rm -Rf nginx-${NGINX_VERSION}.tar.gz; rm -Rf nginx-${NGINX_VERSION}/; rm -Rf ${NPS_VERSION}.tar.gz; rm -Rf release-${NPS_VERSION}-beta.zip; \
    yum remove gcc-c++ pcre-devel zlib-devel make gettext unzip -y;


#setup default nginx.conf
RUN mkdir /etc/nginx/conf.d/

RUN rm /etc/nginx/nginx.conf
COPY ./etc/nginx/nginx.conf /etc/nginx/nginx.conf

ADD ./usr/sbin/launcher /usr/sbin/launcher
RUN chmod +x /usr/sbin/launcher

EXPOSE 80 443

CMD [ "/usr/sbin/launcher" ]